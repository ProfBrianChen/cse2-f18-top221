/*
Tommy Persaud
CSE 002-210
HW01
September 3, 2018
*/
 public class WelcomeClass {
  
  public static void main(String args[]){
    //print Welcome Class to terminal window
    System.out.println("    -----------        ");
    System.out.println("    | WELCOME |        ");
    System.out.println("    -----------        ");
    System.out.println("    ^  ^  ^  ^  ^  ^");
    System.out.println("   / \\/ \\/ \\/ \\/ \\/ \\  ");
    System.out.println("  <-T--O--P--2--2--1-> ");
    System.out.println("   \\ /\\ /\\ /\\ /\\ /\\ /  ");
    System.out.println("    v  v  v  v  v  v");
  }
  
}