/*
Tommy Persaud
CSE 002-210
hw03
September 25, 2018
*/

import java.util.Scanner;
import java.util.Random;

public class CrapsIf{
  
  public static void main (String args[]){
  
    Scanner myScanner = new Scanner(System.in);
    int userChoice; 
    int userD1; //user chosen dice 1
    int userD2; //user chosen dice 2
    int rand1 = (int)(Math.random()*(6)) + 1;
    int rand2 = (int)(Math.random()*(6)) + 1;
    
    System.out.print("Press 1 for to cast your own dice or 2 for randomly generated dice: ");
    userChoice = myScanner.nextInt();
    
    //user chosen dice 
    if (userChoice == 1){
      System.out.print("Enter a number between 1-6 for dice 1: ");
      userD1 = myScanner.nextInt();
      System.out.println();
      System.out.print("Enter a number between 1-6 for dice 2: ");
      userD2 = myScanner.nextInt();
      
     
      
      if(((userD1 + userD2) >= 2) && ((userD1 + userD2) <= 12)){
        //1's
        if ((userD1 == 1) && (userD2 == 1)){
           int score = userD1 + userD1; 
           System.out.println("Snake Eyes! Your score is: " + score );
        }
        else if (((userD1 == 1) && (userD2 == 2)) || ((userD1 == 2) && (userD2 == 1))){
           int score = userD1 + userD1; 
           System.out.println("Ace Deuce! Your score is: " + score );
        }
        else if (((userD1 == 1) && (userD2 == 3)) || ((userD1 == 3) && (userD2 == 1))){
           int score = userD1 + userD1; 
           System.out.println("Easy Four! Your score is: " + score );
        }
        else if (((userD1 == 1) && (userD2 == 4)) || ((userD1 == 4) && (userD2 == 1))){
           int score = userD1 + userD1; 
           System.out.println("Fever Five! Your score is: " + score );
        }
        else if (((userD1 == 1) && (userD2 == 5)) || ((userD1 == 5) && (userD2 == 1))){
           int score = userD1 + userD1; 
           System.out.println("Easy Six! Your score is: " + score );
        }
        else if (((userD1 == 1) && (userD2 == 6)) || ((userD1 == 6) && (userD2 == 1))){
           int score = userD1 + userD1; 
           System.out.println("Seven out! Your score is: " + score );
        }
        //2's
        else if (((userD1 == 2) && (userD2 == 2))){
           int score = userD1 + userD1; 
           System.out.println("Hard four! Your score is: " + score );
        }
        else if (((userD1 == 2) && (userD2 == 3)) || ((userD1 == 3) && (userD2 == 2))){
           int score = userD1 + userD1; 
           System.out.println("Fever five! Your score is: " + score );
        }
        else if (((userD1 == 2) && (userD2 == 4)) || ((userD1 == 4) && (userD2 == 2))){
           int score = userD1 + userD1; 
           System.out.println("Easy six! Your score is: " + score );
        }
        else if (((userD1 == 2) && (userD2 == 5)) || ((userD1 == 5) && (userD2 == 2))){
           int score = userD1 + userD1; 
           System.out.println("Seven out! Your score is: " + score );
        }
        else if (((userD1 == 2) && (userD2 == 6)) || ((userD1 == 6) && (userD2 == 2))){
           int score = userD1 + userD1; 
           System.out.println("Easy Eight! Your score is: " + score );
        }
        //3's
        else if (((userD1 == 3) && (userD2 == 3))){
           int score = userD1 + userD1; 
           System.out.println("Hard six! Your score is: " + score );
        }
        else if (((userD1 == 3) && (userD2 == 4)) || ((userD1 ==4) && (userD2 == 3))){
           int score = userD1 + userD1; 
           System.out.println("Seven out! Your score is: " + score );
        }
        else if (((userD1 == 3) && (userD2 == 5)) || ((userD1 == 5) && (userD2 == 3))){
           int score = userD1 + userD1; 
           System.out.println("Easy Eight! Your score is: " + score );
        }
        else if (((userD1 == 3) && (userD2 == 6)) || ((userD1 == 6) && (userD2 == 3))){
           int score = userD1 + userD1; 
           System.out.println("Nine! Your score is: " + score );
        }
        //4's
        else if (((userD1 == 4) && (userD2 == 4))){
           int score = userD1 + userD1; 
           System.out.println("Hard Eight! Your score is: " + score );
        }
        else if (((userD1 == 4) && (userD2 == 5)) || ((userD1 == 5) && (userD2 == 4))){
           int score = userD1 + userD1; 
           System.out.println("Nine! Your score is: " + score );
        }
        else if (((userD1 == 4) && (userD2 == 6)) || ((userD1 == 6) && (userD2 == 4))){
           int score = userD1 + userD1; 
           System.out.println("Easy Ten! Your score is: " + score );
        }
        //5's
        else if (((userD1 == 5) && (userD2 == 5))){
           int score = userD1 + userD1; 
           System.out.println("Hard Ten! Your score is: " + score );
        }
        else if (((userD1 == 5) && (userD2 == 6)) || ((userD1 == 6) && (userD2 == 5))){
           int score = userD1 + userD1; 
           System.out.println("Yo-leven! Your score is: " + score );
        }
        //6's
        else if (((userD1 == 6) && (userD2 == 6))){
           int score = userD1 + userD1; 
           System.out.println("Boxcars! Your score is: " + score );
        }
      }
      //invald input           
      else{
        System.out.println("Invalid Input! Enter a number from 1-6!");
      }
      
    
    }
    //randomly generated dice 
                 
    else if(userChoice == 2){
      if ((rand1 == 1) && (rand2 == 1)){
           int score = rand1 + rand2; 
           System.out.println("Snake Eyes! Your score is: " + score );
        }
        else if (((rand1 == 1) && (rand2 == 2)) || ((rand1 == 2) && (rand2 == 1))){
           int score = rand1 + rand2; 
           System.out.println("Ace Deuce! Your score is: " + score );
        }
        else if (((rand1 == 1) && (rand2 == 3)) || ((rand1 == 3) && (rand2 == 1))){
           int score = rand1 + rand2; 
           System.out.println("Easy Four! Your score is: " + score );
        }
        else if (((rand1 == 1) && (rand2 == 4)) || ((rand1 == 4) && (rand2 == 1))){
           int score = rand1 + rand2; 
           System.out.println("Fever Five! Your score is: " + score );
        }
        else if (((rand1 == 1) && (rand2 == 5)) || ((rand1 == 5) && (rand2 == 1))){
           int score = rand1 + rand2; 
           System.out.println("Easy Six! Your score is: " + score );
        }
        else if (((rand1 == 1) && (rand2 == 6)) || ((rand1 == 6) && (rand2 == 1))){
           int score = rand1 + rand2; 
           System.out.println("Seven out! Your score is: " + score );
        }
        //2's
        else if (((rand1 == 2) && (rand2 == 2))){
           int score = rand1 + rand2; 
           System.out.println("Hard four! Your score is: " + score );
        }
        else if (((rand1 == 2) && (rand2 == 3)) || ((rand1 == 3) && (rand2 == 2))){
           int score = rand1 + rand2; 
           System.out.println("Fever five! Your score is: " + score );
        }
        else if (((rand1 == 2) && (rand2 == 4)) || ((rand1 == 4) && (rand2 == 2))){
           int score = rand1 + rand2; 
           System.out.println("Easy six! Your score is: " + score );
        }
        else if (((rand1 == 2) && (rand2 == 5)) || ((rand1 == 5) && (rand2 == 2))){
           int score = rand1 + rand2; 
           System.out.println("Seven out! Your score is: " + score );
        }
        else if (((rand1 == 2) && (rand2 == 6)) || ((rand1 == 6) && (rand2 == 2))){
           int score = rand1 + rand2; 
           System.out.println("Easy Eight! Your score is: " + score );
        }
        //3's
        else if (((rand1 == 3) && (rand2 == 3))){
           int score = rand1 + rand2; 
           System.out.println("Hard six! Your score is: " + score );
        }
        else if (((rand1 == 3) && (rand2 == 4)) || ((rand1 ==4) && (rand2 == 3))){
           int score = rand1 + rand2; 
           System.out.println("Seven out! Your score is: " + score );
        }
        else if (((rand1 == 3) && (rand2 == 5)) || ((rand1 == 5) && (rand2 == 3))){
           int score = rand1 + rand2; 
           System.out.println("Easy Eight! Your score is: " + score );
        }
        else if (((rand1 == 3) && (rand2 == 6)) || ((rand1 == 6) && (rand2 == 3))){
           int score = rand1 + rand2; 
           System.out.println("Nine! Your score is: " + score );
        }
        //4's
        else if (((rand1 == 4) && (rand2 == 4))){
           int score = rand1 + rand2; 
           System.out.println("Hard Eight! Your score is: " + score );
        }
        else if (((rand1 == 4) && (rand2 == 5)) || ((rand1 == 5) && (rand2 == 4))){
           int score = rand1 + rand2; 
           System.out.println("Nine! Your score is: " + score );
        }
        else if (((rand1 == 4) && (rand2 == 6)) || ((rand1 == 6) && (rand2 == 4))){
           int score = rand1 + rand2; 
           System.out.println("Easy Ten! Your score is: " + score );
        }
        //5's
        else if (((rand1 == 5) && (rand2 == 5))){
           int score = rand1 + rand2; 
           System.out.println("Hard Ten! Your score is: " + score );
        }
        else if (((rand1 == 5) && (rand2 == 6)) || ((rand1 == 6) && (rand2 == 5))){
           int score = rand1 + rand2; 
           System.out.println("Yo-leven! Your score is: " + score );
        }
        //6's
        else if (((rand1 == 6) && (rand2 == 6))){
           int score = rand1 + rand2; 
           System.out.println("Boxcars! Your score is: " + score );
        }
      
    }
    //if the user enters a invalid choice
    else{
      System.out.println("Invalid input! Please enter 1 or 2");
    }
    
    
    
    
  } //end of main method 
  
  
} //end of class 
