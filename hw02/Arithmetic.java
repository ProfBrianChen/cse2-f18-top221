/*
Tommy Persaud
CSE 002-210
hw02
September 11, 2018
*/

/*
the purpose of this program is to calculate the total cost of a purchase with and without tax, and calculate the sales tax seperately
*/

public class Arithmetic {
  
  public static void main(String args[]){
  
  //Number of pairs of pants
  int numPants = 3;
  //Cost per pair of paints
  double pantsPrice = 34.98;
    
  //Number of sweatshirts
  int numShirts = 2;
  //Cost per shirt
  double shirtPrice = 24.99;
  
  //Number of belts
  int numBelts = 1;
  //cost per belt
  double beltCost = 33.99;
  
  //the tax rate
  double paSalesTax = 0.06; 
  
  //total cost of pants 
  double totalCostOfPants;
  //determine the total cost of pants using the number of paints times the price of the pants 
  totalCostOfPants = numPants*pantsPrice;
    
  //total cost of shirts
  double totalCostOfShirts;
  //determine the total cost of shirts using the number of shirts times the price of the shirts
  totalCostOfShirts = numShirts*shirtPrice;
    
  //total cost of belts
  double totalCostOfBelts;
  //determine the total cost of belts using the number of belts times the price of the belts
  totalCostOfBelts = numBelts*beltCost;
    
  //total cost of the entire purchase   
  double totalCostOfPurchase;
  //determine the price of the purchase before taxes  
  totalCostOfPurchase = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
  
  //determine the total sales taxes
  double totalSalesTax;
  //the total sales tax is the total cost of the purchase times the sales tax
  totalSalesTax = (totalCostOfPurchase * paSalesTax);
  //round the decimal
  totalSalesTax = ((int)(totalSalesTax*100))/100.0;
    
  //determine the total cost of the purchase included the tax
  double totalCostOfPurchaseWithTax;
  //the total cost of the purchase with taxes is equalt to the total cost of the purchase plus the sales tax  
  totalCostOfPurchaseWithTax = totalCostOfPurchase + totalSalesTax;
   
  //printing the total cost of the pants
  System.out.println("The total cost of the pants purchased without tax is: $" + totalCostOfPants);
  //printing the tax of the pants purchaed
  System.out.println("The total tax of the pants purchases is: $" + ((int)(totalCostOfPants * paSalesTax)*100)/100.0);  
  //printing the total cost of the pants with tax  
  System.out.println("The total cost of the pants purchased with tax is: $" + (totalCostOfPants + ((int)(totalCostOfPants * paSalesTax)*100)/100.0));
  //printing the total cost of the shirts
  System.out.println("The total cost of the shirts purchased without tax is: $" + totalCostOfShirts);
  //printing the tax of the shirts purchased
  System.out.println("The total tax of the shirts purchased is: $" + ((int)(totalCostOfShirts * paSalesTax)*100)/100.0);  
  //printing the total cost of the shirts with tax
  System.out.println("The total cost of the shirts purchased with tax is: $" + (totalCostOfShirts + ((int)(totalCostOfShirts * paSalesTax)*100)/100.0));
  //printing the total cost of the belts
  System.out.println("The total cost of the bets without tax is: $" + totalCostOfBelts);  
  //printing the tax of the belts purchased
  System.out.println("The total tax of the belts purchased is: $" + ((int)(totalCostOfBelts * paSalesTax)*100)/100.0);  
  //printing the total cost of the belts with tax
  System.out.println("The total cost of the bets without tax is: $" + (totalCostOfBelts + ((int)(totalCostOfBelts * paSalesTax)*100)/100.0));  
  //printing the cost of the purchase before sales tax 
  System.out.println("The total cost of the purchase before taxes is: $" + totalCostOfPurchase);
  //printing the cost of the sales tax from the total cost of the purchase  
  System.out.println("The total sales tax is: $" + totalSalesTax);
  //printing the total cost of the purchase including the sales tax
  System.out.println("Thte total cost of the purchase with tax is: $" + totalCostOfPurchaseWithTax);  
    
    
  }//end of main method 
  
  
} //end of class 