/*
 * Tommy Persaud
 * Professor Kalafut
 * CSE 2
 * 12/4/18
 */

/*
 * the purpose of this program is to simulate playing Tic-Tac-Toe with two players
 */ 

import java.util.Scanner;

public class HW10{ 

 public static void main(String[] args) {
  
  //variable declarations 
  final int NUM_ROW = 3;
  final int NUM_COL = 3;
  int row;
  int col;
  char userTurn = 'X';
  Scanner userInput = new Scanner(System.in);
  
  
  char[][] board = new char[NUM_ROW][NUM_COL]; //create tic tac toe board 
  char index = '1'; //start the index of the board at 1 
  
  //populate the board with numbers 
  for (int i = 0; i <NUM_ROW; i++) {
   for (int j = 0; j < NUM_COL; j++) {
    board[i][j] = index++;
   }
  }
  //prints the board layout
  printLayout(board);
  System.out.println("Enter numbers from 1-3!");
  while (gameStatus(board) != true) {

   //allow players to enter a row 
   System.out.println("Player " + userTurn + " enter a row: ");
   row = userInput.nextInt()-1;
   
   //allows users to enter a column
   System.out.println("Player " + userTurn + " enter a row: ");
   col = userInput.nextInt()-1; 

   //check if the spot is taken or not
   if (board[row][col] == 'X' || board[row][col] == 'O') {
    System.out.println("Spot taken, enter again! ");
    continue;
   }

   //declare the player 
   board[row][col] = userTurn;
   printLayout(board);

   //players switch after each turn is made 
   if (userTurn == 'O') {
    userTurn = 'X';
   } 
   else 
   {
    userTurn = 'O';
   }
   
  }
  
  //checks to see if combination is false, therefore a draw must occur
  if (gameStatus(board) == false) {
    System.out.println("The game is a draw. Please try again.");
  }
  
  //swap back to winner player!
  if (userTurn == 'O') {
   userTurn = 'X';
  } else {
   userTurn = 'O';
  }
  System.out.println("Player " + userTurn + " has won!"); //print the winning player 
  
  
 }//end of main method 
 
 
 //method determines the status of the game
 public static Boolean gameStatus(char[][] board) {

  //check all the possible winning combinations 
  if ((board[0][0] == board[0][1] && board[0][0] == board[0][2]) || 
   (board[1][0] == board[1][1] && board[1][0] == board[1][2]) || 
   (board[2][0] == board[2][1] && board[2][0] == board[2][2]) || 
   (board[0][0] == board[1][0] && board[0][0] == board[2][0]) || 
   (board[0][1] == board[1][1] && board[0][1] == board[2][1]) || 
   (board[0][2] == board[1][2] && board[0][2] == board[2][2]) || 
   (board[0][0] == board[1][1] && board[0][0] == board[2][2]) || 
   (board[2][0] == board[1][1] && board[2][0] == board[0][2])) { 
   return true;
  } 
  else {
   return false;
  }
 }//end of gameStatus method 
 
 
 
  //method prints the layout of the board
 public static void printLayout(char[][] board) {
  for (int i = 0; i < board.length; i++) { 
   for (int j = 0; j < board[i].length; j++) 
   { 
    if (j == board[i].length - 1) 
      System.out.print(board[i][j]);
    else 
      System.out.print(board[i][j] + " ");
   }
   System.out.println();//new line
  }
 }//end of print layout method 
 
 
}//end of class 
