/*
Tommy Persaud
CSE 002-210
lab04
September 19, 2018
*/

/*
the purpose of the lab is generate a random card and assign a suit and value to it
*/

import java.util.Random;

public class CardGenerator {
  
  public static void main(String args[]){
    String suit;
    String identity;
    
    int rand = (int)(Math.random()*(52)) + 1; //generate random number
    
    //this if statement is to determine whether the randomly generated number is a diamonds and the number value 
    if ((rand >= 1) && (rand <= 13)){ //check if the card suit is diamonds
       suit = ("Diamonds"); //assign the string suit to diamonds 
       if (rand == 1){ //check if the number is an ace 
          identity = ("Ace"); //assign the string identity to ace 
          System.out.println("You picked the " + identity + " of " + suit); //print the identity of the card and the suit
       }
       else if (rand == 11){ 
          identity = ("Jack");
          System.out.println("You picked the " + identity + " of " + suit);
       }
       else if (rand == 12){
          identity = ("Queen");
          System.out.println("You picked the " + identity + " of " + suit);
       }
       else if (rand == 13){
          identity = ("King");
          System.out.println("You picked the " + identity + " of " + suit);
       }
      else {
        System.out.println("You picked the " + rand%13 + " of " + suit);
      }
    }
    //this if statement is to determine whether the randomly generated number is a clubs and the number value 
      if ((rand >= 14) && (rand <= 26)){
       suit = ("Clubs");
        if (rand == 14){
          identity = ("Ace");
          System.out.println("You picked the " + identity + " of " + suit);
       }
       else if (rand == 24){
          identity = ("Jack");
          System.out.println("You picked the " + identity + " of " + suit);
       }
       else if (rand == 25){
          identity = ("Queen");
          System.out.println("You picked the " + identity + " of " + suit);
       }
       else if (rand == 26){
          identity = ("King");
          System.out.println("You picked the " + identity + " of " + suit);
       }
      else {
        System.out.println("You picked the " + rand%13 + " of " + suit);
      }
    }
    //this if statement is to determine whether the randomly generated number is a hearts and the number value 
      if ((rand >= 27) && (rand <= 39)){
       suit = ("Hearts");
        if (rand == 27){
          identity = ("Ace");
          System.out.println("You picked the " + identity + " of " + suit);
       }
       else if (rand == 37){
          identity = ("Jack");
          System.out.println("You picked the " + identity + " of " + suit);
       }
       else if (rand == 38){
          identity = ("Queen");
          System.out.println("You picked the " + identity + " of " + suit);
       }
       else if (rand == 39){
          identity = ("King");
          System.out.println("You picked the " + identity + " of " + suit);
       }
      else {
        System.out.println("You picked the " + rand%13 + " of " + suit);
      }
    }
    //this if statement is to determine whether the randomly generated number is a spades and the number value 
      if ((rand >= 40) && (rand <= 52)){
       suit = ("Spades");
        if (rand == 40){
          identity = ("Ace");
          System.out.println("You picked the " + identity + " of " + suit);
       }
       else if (rand == 50){
          identity = ("Jack");
          System.out.println("You picked the " + identity + " of " + suit);
       }
       else if (rand == 51){
          identity = ("Queen");
          System.out.println("You picked the " + identity + " of " + suit);
       }
       else if (rand == 52){
          identity = ("King");
          System.out.println("You picked the " + identity + " of " + suit);
       }
      else {
        System.out.println("You picked the " + rand%13 + " of " + suit);
      }
    }
    
    
  } //end of main method 
}//end of main class  
  
