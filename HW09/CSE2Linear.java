/*
 * Tommy Persaud 
 * Professor Kalafut
 * CSE 2
 * hw09
 */

/*
 * the purpose of this program is to practice searching through single dimensional arrays 
 */ 

import java.util.Scanner; 

public class CSE2Linear{
  public static void main(String[] args){
    
    //variables and other declarations 
    final int ARRAY_SIZE = 15;  //array size 
    int[] grades = new int[ARRAY_SIZE]; //array declaration
    Scanner user = new Scanner(System.in); //scanner initialization
    int i; //number of interations used in loops
    int key; //key for binary and or linear search 
    
    //prompt the user to enter 15 integer values
    System.out.println("Enter 15 Ascending Integers from 0-100 for Student Grades: ");
    
      //populate the array with user entered values
      for(i = 0; i < ARRAY_SIZE; i++){
        //checks if the user enteres a integer 
        while(!user.hasNextInt()){
          System.out.println("Invalid Input, Enter a Integer!");
          String junk = user.next();
        }
        //checks if the user enters a value within range 
        while(!(grades[i]>= 0 && grades[i]<=0)){
          System.out.println("Invalid Input, value entered is out of range!");
          String junk = user.next();
        }
        grades[i] = user.nextInt(); 
        
      }//end of populating loop
    
    printing(grades);//print the array
    //prompt the user to enter a grade to search for 
    System.out.print("Enter a grade to search for: ");
    //assign the key to user input  
    key = user.nextInt();
    
    //checking if the key is indeed valud 
    i = binarySearch(grades,key);
    if(i != -1){
      System.out.println(key + " was found in the list with " + i +" iterations");
    }
    //scrambled the array of grades
    grades = scrambleArray(grades);
    System.out.println("Scrambled:");
    //print the scrambled array of grades 
    printing(grades);
    //prompt use to enter the number they want (key)
    System.out.print("Enter the grade you are looking for: ");
    //assigning the user entered value to key 
    key = user.nextInt();
    //search the key using linear seach 
    i = linearSearch(grades,key);
    //key/user value is found 
    if(i != -1){
      System.out.println(key + " was found in the list with " + i +" iterations");
    }
  }//end of main method 
 

  //method to print print array
  public static void printing(int[] array){
    for(int i = 0; i<array.length; i++){
      System.out.print(array[i] + " ");
    }//end of printing loop
  }//end of printing method 
  
  
  //binary search method code from zybooks 
  public static int binarySearch(int[] list, int key){
      int mid; //middle of the search index 
      int low; //lowest index 
      int high; //highest index 
      int count = 0; //counter to find the number of interations
      low = 0; //initlize low to first element
      high = list.length-1; //high to last element 
      
      //interate through the sorted array to find the desired number (key)
      while (high >= low) {
         count++; //counter 
         mid = (high + low) / 2;
         if (list[mid] < key) {
            low = mid + 1;
         } 
         else if (list[mid] > key) {
            high = mid - 1;
         } 
         else {
            return count++; //return the number of iteratiosn 
         }
      }
      System.out.println(key + " was not found in the list with " + count + " interations.");
      return -1; // not found
  }//end of binary search method 

  //method to scramble array
  public static int[] scrambleArray(int[] array){
      //loop to randomly swap the numbers 
      for(int i = 0; i<array.length; i++){
        int random = (int)((Math.random()*(15-i))+1);
        //swapping algorithm
        int temp = array[random];
        array[random] = array[i];
        array[i] = temp;  
      }
      return array; //return scrambled array 
  }//end of scrambling array 

  //linear search method code from zybooks 
  public static int linearSearch(int[] array, int key){
    int count = 0;
    for(int i = 0; i<= array.length-1; i++){
      count++;
      if(array[i] == key){
        return count;
      }
    }
    System.out.println(key + " was not found with " + count + " interations.");
    return -1; //key is not found
  }//end of linear search method 
  
}//end of class 

