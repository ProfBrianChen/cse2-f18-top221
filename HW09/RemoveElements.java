/*
 * Tommy Persaud
 * Professor Kalafut
 * CSE 002-210
 * hw09
 */ 

/*
 * the purpose of this program is to practice searching through single dimensional arrays 
 */ 

import java.util.Scanner;

public class RemoveElements{
  
  public static void main(String [] arg){
    
    Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer="";
    do{
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);
   
      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num,index);
      String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
      
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);
   
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
    }while(answer.equals("Y") || answer.equals("y"));
    
  }//end of main method 
  
  //list Array 
  public static String listArray(int num[]){
    String out="{";
    for(int j=0;j<num.length;j++){
      if(j>0){
        out+=", ";
      }
      out+=num[j];
    }
    out+="} ";
    return out;
  }//end of listArray method 

  
  //randomInput method generates an array of 10 random integers between 0 to 9
   public static int[] randomInput(){
     //declare array size 
     final int ARRAY_SIZE = 10;
     //array declaration
     int[] randArr = new int[ARRAY_SIZE];
     //loop array to populate with random values 
     for(int i=0; i<ARRAY_SIZE; i++){
       randArr[i] = (int)(Math.random()*(10));
     }
     //return the randomly generated array 
     return randArr;
   }//end of method 
   
   //delete method (list, pos), creates a new array that has one member fewer than list called list, and can be composed of
   // all of the same memebers except the member in the position pos
   public static int[] delete(int[] list, int pos){
     //make sure the position in within bounds 
     if(pos >= 10){
       System.out.println("Invalid Index!"); //print error message 
       return list; //return same array as orignal
     }
     //new array with the same length of inputted array 
     int[] array = new int[list.length - 1];
     //loop to copy array values over 
     for(int i=0; i<pos; i++){
       array [i] = list[i];
     }
     //copy values 
     for(int i=pos; i<array.length; i++){
       array[i] = list[i+1];
     }
     return array;//return new copied array 
     
   }//end of method 
   
   
   //remove method deletes all the elements that are equal to target,
   //returning a new list without all those new elements 
   public static int[] remove(int[] list, int target){
    //count for number of elements that match the target element are found 
    int count = 0;
    int counter = 0;
    
    //loop to check if target is found 
    for(int i=0;i<list.length;++i){
      if(target == list[i]){
      //increment count 
      count++;
      }
    }
   //if target did not repeat 
   if(count == 0){
     //print not found message
     System.out.println("Element " + target + " was not found!");
     //return the same array that was originally entered 
     return list;
   }
   
   System.out.println("Element " + target + " has been found");
   //new output array 
   int[] array = new int[list.length-count];
   //iterates through array and increments a new counter is the trget is found 
   for(int i=0; i<list.length; i++){
     //checks if the target is found 
     if(target == list[i]){
      counter++;
      //forces loop to keep repeated until it needs to be broken 
      continue;
    }
    //makes new array depending the number of times the target is found through counter 
    array[i-counter] = list[i];
   }
   return array;//return the new array formed 
   
  }//end of method 
  
}//end of class 
