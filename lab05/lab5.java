/*
Tommy Persaud
CSE 002-210
lab05
10-8-18
*/

/**
 * the purpose of this program is to determine the user class information based on a variety of inputs 
 * 
 **/

import java.util.Scanner;
public class lab5{
 public static void main(String[] args) {

  //Scanner declarations 
  Scanner scnr = new Scanner(System.in);
  Scanner scnr1 = new Scanner(System.in);
  Scanner scnr2 = new Scanner(System.in);
  Scanner scnr3 = new Scanner(System.in);
  Scanner scnr4 = new Scanner(System.in);
  Scanner scnr5 = new Scanner(System.in);
  
  //user enters the course number 
  int courseNum;
  System.out.print("Enter the course number: ");
  while (!scnr.hasNextInt())
  {
     System.out.print("Invalid Number!!! Enter an Integer: ");
     String junkWord = scnr.next();
     
  }
 courseNum = scnr.nextInt(); 
  
  //user enters the name of the department 
  String department;
  System.out.print("Enter the name of the department: ");
 // while (!scnr1.hasNextLine())
  //{   
    //  System.out.print("Invalid name!!! Enter again: ");
      //String junkWord1 = scnr1.next();      
  //e}
  department = scnr1.nextLine();
  
  
  //user enters the number of times the course meets
  int numMeet;
  System.out.print("Enter the number of the times the class meets per week: ");
  while (!scnr2.hasNextInt())
  {
      System.out.print("Invalid Number!!! Enter an Integer: ");
      String junkword2 = scnr2.next();
  }
  numMeet = scnr2.nextInt();
  
  
  //user enters the time the class starts in millitary time
  int time;
  System.out.print("Enter the time the class starts in millitary time: ");
  while (!scnr3.hasNextInt())
  {
    System.out.print("Invalid Number!!! Enter an Integer: ");
    String junkword3 = scnr3.next();
  }
  time = scnr3.nextInt();
  
  //user enters the name of the instructor
  String instructorName;
  System.out.print("Enter the name of the instructor: ");
 // while (!scnr4.hasNextLine())
 // {   
   //   System.out.print("Invalid name!!! Enter again: ");
    //  String junkWord4 = scnr4.next();      
  //}
  instructorName = scnr4.nextLine();
  
  
  //user enters the number of students 
  int numStudents;
  System.out.print("Enter the number of students: ");
  while (!scnr5.hasNextInt())
  {
     System.out.print("Invalid Number!!! Enter an Integer: ");
     String junkword4 = scnr5.next();
  }
  numStudents = scnr5.nextInt();
  
  
  //all print statements from user inputs 
  System.out.println("Course number is " + courseNum);
  System.out.println("The name of the department is " + department);
  System.out.println("Number of times the course meets is " + numMeet);
  System.out.println("The time the class meets is " + time);
  System.out.println("The name of the instructor is " + instructorName);
  System.out.println("The number of students is " + numStudents);
  
  
  
 }
}

