/*
Tommy Persaud
CSE 002-210
lab02
September 5, 2018
*/

/*the purpose of this program is to measure speed and distance
of a bicyle trip. this will record the time elapsed in seconds and 
the number of rotations of the front wheel during that time.
*/

public class Cyclometer {

  public static void main(String args[]){
  
  int secsTrip1 = 480; //number of seconds for trip 1
  int secsTrip2 = 3220; //number of seconds for trip 2 
  int countsTrip1 = 1561; //number of rotations for trip 1
  int countsTrip2 = 9037; //number of rotations for trip 2
  
  //constant variables
  double wheelDiameter = 27.0,
  PI = 3.14159,
  feetPerMile = 5280,
  inchesPerFoot = 12,
  secondsPerMinute = 60;
  double distanceTrip1, distanceTrip2, totalDistance;
  //print the time trip 1 took
  System.out.println("Trip 1 took " + 
    (secsTrip1/secondsPerMinute) + " minutes and had " +
    countsTrip1 + " counts.");
  //print the time trip 2 took  
  System.out.println("Trip 2 took " + 
    (secsTrip2/secondsPerMinute) + " minutes and had " +
    countsTrip2 + " counts.");
  //calculating the distance for trip 1 
  distanceTrip1 = countsTrip1 * wheelDiameter * PI; //gives distance in inches
  distanceTrip1 /= inchesPerFoot * feetPerMile; //gives distance in miles
  //calculating the distance for trip 2
  distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile;
  //calculating the total distance
  totalDistance = distanceTrip1 + distanceTrip2;
  
  //print out the output data
  System.out.println("Trip 1 was " + distanceTrip1 + " miles");
  System.out.println("Trip 2 was " + distanceTrip2 + " miles");
  System.out.println("The total distance was " + totalDistance + " miles");
  
 
  } //end of main method

} //end of class 