/*
 *Tommy Persaud
 *Professor Kalafut
 *CSE 002-210
 *hw06 
 */

/*this program will print a secreat messafe X */

import java.util.Scanner;

public class EncryptedX {
  public static void main (String [] args){
   
  Scanner input = new Scanner(System.in);
  
  int rows;
  //prompt user input
  System.out.println("Enter an integer from 0-100");
  //makes sure user input is valid
  while (!input.hasNextInt())
  {
   System.out.print("Invalid Number!!! Enter an Integer from 0-100: ");
   String junkWord = input.next();
  }
  rows = input.nextInt();  
  
    //loop will print the numbers of rows 
    for(int i = 0; i <= rows; i++){
    //loop for printing the spaces and the *'s
    for(int j = 0; j <=rows; j++){
      //checks if the i and j are in the same position
      if (j == rows - i || j == i){
        System.out.print(" ");
      }
      else{ 
       System.out.print("*"); 
      }
    }
    //prints each line
    System.out.println();
  }
  
    
    
  }//end of main method 
  
}//end of class
