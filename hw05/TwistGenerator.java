/*
 *Tommy Persaud
 *Professor Kalafut
 *CSE 002-210
 *hw05 
 */

/*
 * the purpose of this program is to print out a simple "twist" on the screen
 */ 

import java.util.Scanner;

public class TwistGenerator{
  public static void main(String [] args){
    
    
    //integer delcaration
    int length;
    //allowing the user to enter a positive integer 
    Scanner input = new Scanner(System.in);
    System.out.print("Enter a positive integer: ");
    
    //check the user input is an integer
    while(!input.hasNextInt()){
      System.out.println("Invalid Input. Enter and a positive integer: ");
      String junk = input.next();   
    }
    length = input.nextInt();
    
    //check the length of the user input to be a positive number
    //allows the user to enter the number again if the value previously entered is negative
    while(length <=  0){
      System.out.println("Number is not positive! Enter a positive number: ");
      length = input.nextInt();
    }
    //"a twist is always three lines long"
    //length = length/3;
    
    //using three for loops, print a twist 
    for(int i = 1; i <= length; ++i){
       System.out.print("\\ /");
       }
    System.out.println();
    for(int i = 1; i <= length; ++i){
       System.out.print(" X ");
    }
    System.out.println();
    for(int i = 1; i <= length; ++i){
       System.out.print("/ \\");
    }
  
    
    
  } //end of main method 
} //end of class
