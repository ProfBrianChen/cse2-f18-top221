/*
Tommy Persaud
CSE 002-210
hw03
September 18, 2018
*/

/*
the purpose of this hw is to calculate the volume inside of a pyramid
*/

import java.util.Scanner;

public class Pyramid {
  
  public static void main(String args[]){
    
  Scanner user = new Scanner(System.in); //declare a new scanner
  double height; //declare the height of the pyramid 
  double side; //declare the side of the pyramid
  
   
  System.out.print("The square side of the pyramid is: "); //ask the user to 
  side = user.nextDouble(); //allow the user to enter the side of the pyramid
   
  System.out.print("The height of the pyramid is: "); //ask the user to print the height of the pyramid 
  height = user.nextDouble(); //allow the user input to enter the height of the pyramid
    
  double volume = ((side*side)*(height))/3; //formula for the volume of a pyramid
  System.out.print("The volume inside the pyramid is: " + volume); //print the volume inside the pyramid
       
  } //end of main method 
  
} //end of class
