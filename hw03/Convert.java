/*
Tommy Persaud
CSE 002-210
hw03
September 18, 2018
*/

/*
the purpose of this program is to calulate the quantity of rain into cubic miles from a user input
*/
import java.util.Scanner; //import the scanner class into java 

public class Convert {
  
  public static void main(String args[]){
    
  Scanner myScanner = new Scanner(System.in); //declare a new scanner
  double acres; //declare acres
  double rainfall; //declare rainfall
    
  System.out.print("Enter the affected area in acres: "); //print a statement asking the user to enter the number of acres of land covered 
  acres = myScanner.nextDouble(); //allow the user to input the acres of land 
  System.out.print("Enter the rainfall in the affected area: "); //print a statement asking the user to enter the inches of rainfall
  rainfall = myScanner.nextDouble(); //allow the user to input the inches of rainfall 
   
  double inchesToMiles = 0.0000157828; //inches to miles conversion
  double acresToMiles = 0.0015625; //acres to miles conversion 
  rainfall = (rainfall*inchesToMiles); //convert rainfall from inches to miles
  acres = (acres*acresToMiles); //convert acres to miles
  
  System.out.println((acres*rainfall) + "cubic miles"); //print the quantity of rain in cubic miles 
    
  } //end of main method 
  
}// end of main class
  