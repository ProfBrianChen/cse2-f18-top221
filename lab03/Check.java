import java.util.Scanner;

/*
Tommy Persaud
CSE 002-210
lab03
September 12, 2018
*/

/* the purpose of this lab is to use the Scanner class to obtain from the user the original cost of the check, the percentage tip 
they wish to pay, and the number of ways the check will be split. 
Then determine how much each person in the group needs to spend in order to pay the check.
*/

public class Check {
  //main method required for every java program
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in); //declare new scanner
    
    //prompt the user for the original cost of the check
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    //accept the user input
    double checkCost = myScanner.nextDouble();
    
    /*prompt the user for the tip percentage that they wish to pay as a whole number
    in the form xx and accep the input */
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx)");
    
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100; //we want to convert the percentage into a decimal value
      
    /* prompt the user for the number of people that wne to dinner and accept the input. 
    this will be the same  number of ways that the check will be split*/
    
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt(); 
    
    /*print the output. output the amount that each memeber of the group needs to spend in  order
    to pay the check 
    */
    
    double totalCost;
    double costPerPerson;
    int dollars, //whole dollar amount of cost 
        dimes, pennies; //for storing digits 
               //to the right of the decimal point
               //for the cost$
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    //get the whoe amount, dropping decimal fraction
    dollars = (int)costPerPerson;
    
    //get dimes amount, e.g,
    //(int)(6.73 * 10) % -> 67 % 10 -> 7
    // where the % (mod) operator returns the remainder
    // after the division: 583 % 100 -> 83, 27 % 5 ->
    dimes = (int)(costPerPerson * 10) % 10;
    pennies = (int)(costPerPerson * 100) % 10;
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); //print the amount each person owes
      
  } //end of main method 
  
  
  
} //end of class 