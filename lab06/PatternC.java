/*
 *Tommy Persaud
 *CSE 002-210
 *Professor Kalafut
 *lab06 PatternC
 */

import java.util.Scanner;

public class PatternC{
  public static void main(String [] args){
    
    Scanner input = new Scanner(System.in);
    
    int rows;
    //prompt user
    System.out.println("Enter an integer from 1-10");
    //check the user input to make sure the input is valid  
    while (!input.hasNextInt())
    {
    System.out.print("Invalid Number!!! Enter an Integer: ");
    String junkWord = input.next();
    }
    rows = input.nextInt(); 
     
    for(int i = 1; i <= rows; i++){
      //loop creates right allignment
      for(int j = rows; j >=i; j--){
        System.out.print(" ");
      }
      //loop prints numbers in reverse order
      for(int k = i; k>= 1; k--){
        System.out.print(k);
      }
      //prints a line between every loop
       System.out.println();
    }
   
    
    
    
  } //end of main method 

} //end of class