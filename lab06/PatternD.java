/*
 *Tommy Persaud
 *CSE 002-210
 *Professor Kalafut
 *lab06 PatternD
 */


import java.util.Scanner;

public class PatternD {
  public static void main(String [] args){
    
    Scanner input = new Scanner(System.in);
    
    int rows;
    //prompt user input
    System.out.println("Enter an integer from 1-10");
    //make sure input is valid 
    while (!input.hasNextInt())
    {
    System.out.print("Invalid Number!!! Enter an Integer: ");
    String junkWord = input.next();
    }
    rows = input.nextInt(); 
    //loop for number of rows
    for(int i = rows; i >= 1; i--){
      //loop ofr printig numbers 
      for(int j = i; j >=1; j--){
        System.out.print(j);
      }//end of second for loop
      System.out.println();
    }//end of first for loop
    
  } //end of main method
  
} //end of class 