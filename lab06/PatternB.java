/*
 *Tommy Persaud
 *CSE 002-210
 *Professor Kalafut
 *lab06 PatternB
 */


import java.util.Scanner;

public class PatternB{
  public static void main(String [] args){
    
    Scanner input = new Scanner(System.in);
    
    int rows;
    //prompt user input 
    System.out.println("Enter an integer from 1-10");
    //make sure user input is valid 
    while (!input.hasNextInt())
    {
    System.out.print("Invalid Number!!! Enter an Integer: ");
    String junkWord = input.next();
    }
    rows = input.nextInt(); 
    //loop for rows 
    for(int i = rows; i >= 1; --i){
      //loop for printing numbers 
      for(int j = 1; j <=i; j++){
        System.out.print(j);
      }//end of second for loop
      System.out.println();
    }//end of first for loop
    
  } //end of main method
  
} //end of class 